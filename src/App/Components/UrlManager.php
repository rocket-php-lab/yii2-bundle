<?php

namespace RocketLab\Bundle\App\Components;

/**
 * Class UrlManager
 *
 * Для совместимости с Brigde
 *
 * @package RocketLab\Bundle\App\Components
 */
class UrlManager extends \yii\web\UrlManager
{
    public $languages = [];
}
