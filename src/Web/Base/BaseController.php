<?php

namespace RocketLab\Bundle\Web\Base;

use RocketLab\Bundle\Web\Traits\BehaviorTrait;
use yii\rest\Controller;

class BaseController extends Controller
{

    use BehaviorTrait;

}
